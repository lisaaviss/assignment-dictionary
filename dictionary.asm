%ifndef LABLE_LENGHT
    %define LABLE_LENGHT 8
%endif

extern string_equals

section .text

        ; 1 арг указатель на строку ключа с нулевым завершением.
        ; 2 арг указатель на последнее слово в словаре. Имея указатель на последнее определенное слово, 
        ;   мы можем следовать последовательным ссылкам, чтобы перечислить все слова в словаре.
        ; find_word будет циклически проходить через весь словарь, сравнивая данный ключ с каждым ключом в словаре. 
        ; Если запись не найдена, она возвращает ноль; в противном случае она возвращает адрес записи.
global find_word


find_word:
    push rdi
    push rsi

    xor rax, rax
    .loop:
        cmp rsi, 0
        je .exit
        ; получение ключа
        push rsi            
        add rsi, LABLE_LENGHT
        call string_equals
        pop rsi
        ;
        cmp rax, 0
        jne .found
        mov rsi, [rsi]
        jmp .loop

    .found:
        mov rax, rsi
    .exit:
        pop rsi
        pop rdi
        ret

