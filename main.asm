%define STDOUT 1
%define STDERR 2
%define BUFF_LEN 255

%ifndef LABLE_LENGHT
    %define LABLE_LENGHT 8
%endif

%include "words.inc"

extern exit
extern read_word
extern print_string
extern find_word
extern string_length

section .data
    NPE: db "NotFountException", 0

section .text

        ; Простая функция _start. Он должен выполнить следующие действия:
        ; • Считайте входную строку в буфере длиной не более 255 символов.
        ; • Попробуйте найти этот ключ в словаре. Если найдено, выведите соответствующее значение. Если нет,выведите сообщение об ошибке.

global _start
_start:
    call read_word

    mov rdi, rax
    mov rsi, index
    
    call find_word

    cmp rax, 0
    je .not_found

    add rax, LABLE_LENGHT
    mov rdi, rax
    call string_length
    lea rdi, [rdi+rax+1]

            ;mov rdi, rax
    mov rsi, STDOUT
    call print_string

    jmp .finish

    .not_found:
        mov rdi, NPE
        mov rsi, STDERR
        call print_string

        jmp .finish

    .finish:
        call exit

